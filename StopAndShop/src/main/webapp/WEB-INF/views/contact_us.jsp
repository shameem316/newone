<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<style>
body {
	font-family: "Open Sans", Helvetica, Arial, sans-serif;
	line-height: 30px;
}

.aboutouter {
	max-width: 700px;
	width: 100%;
	margin: 0 auto;
	position: relative;
}

#aboutinner input[type="text"], #aboutinner input[type="email"],
	#aboutinner input[type="tel"], 
	#aboutinner textarea, #aboutinner button[type="submit"] {
	font: 400 12px/16px "Open Sans", Helvetica, Arial, sans-serif;
}

#aboutinner {
	padding: 25px;
	margin: 50px 0;
}

#aboutinner h3 {
	color: black;
	font-size: 30px;
}

#aboutinner h4 {
	margin: 5px 0 15px;
	display: block;
	font-size: 13px;
}

fieldset {
	border: medium none !important;
	margin: 0 0 10px;
	min-width: 100%;
	padding: 10;
	width: 100%;
}

#aboutinner input[type="text"], #aboutinner input[type="email"],
	#aboutinner input[type="tel"], 
	#aboutinner textarea {
	width: 100%;
	border: 1px solid #CCC;
	background: #FFF;
	margin: 0 0 5px;
	padding: 10px;
	box-shadow: 5px 5px #D1D0CE;
}

#aboutinner textarea {
	height: 100px;
	max-width: 100%;
	resize: none;
	box-shadow: 5px 5px #D1D0CE;
}

#aboutinner button[type="submit"] {
	cursor: pointer;
	width: 25%;
	border: none;
	background: #0CF;
	color: #FFF;
	margin: 0 0 5px;
	padding: 10px;
	font-size: 15px;
}

#aboutinner button[type="submit"]:hover {
	background: #09C;
	-webkit-transition: background 0.3s ease-in-out;
	-moz-transition: background 0.3s ease-in-out;
	transition: background-color 0.3s ease-in-out;
}

#aboutinner button[type="submit"]:active {
	box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.5);
}

#aboutinner input:focus, #aboutinner textarea:focus {
	outline: 0;
	border: 1px solid skyblue;
}

.text-danger {
	color: red;
	font-weight: bold;
}
</style>

<body>
	<div class="aboutouter" id="contact">
		<form:errors path="contact.*"></form:errors>
		<form id="aboutinner" action="/LoginApplication/contact_us"
			method="post">
			<center>
				<h3>
					<b><font color="black" size="6">Contact Us</font></b>
				</h3>
				<h4>
					<font color="black" size="3">Getting in touch is so
						Easy.&nbspWe are there for you Anytime...</font>
				</h4>
				 
				 <h4 ><font color="green">${contactMsg}</font></h4>
				 
			</center>
			<fieldset>
				<input placeholder="Your name" type="text" id="name" name="name"
					onkeypress="return onlyAlphabets(event,this);" required="true" style="height:50px;"/>
				 <br />
			</fieldset>
			<fieldset>
				<input placeholder="Your Email Address" type="email" id="email"
					name="email" required="true" style="height:50px;" />  <br />
			</fieldset>
			<fieldset>
				<input placeholder="Your Phone Number" type="tel" id="phone"
					name="phone" maxlength="10" onkeypress="return card(event)"
					required="true" style="height:50px;"/> 
				<br />
			</fieldset>

			<fieldset>
				<input placeholder="Your Subject" type="text" id="email"
					name="subject" required="true" style="height:50px;"/>  <br />
			</fieldset>
			<fieldset>
				<textarea placeholder="Type your Message Here...." type="text" id="msg"
					name="msg" required="true"></textarea>
			</fieldset>
			<br />
			<br />
			<fieldset>
				<center>
					<button name="submit" type="submit" id="aboutinner-submit"
						data-submit="...Sending">Submit</button>
				</center>
			</fieldset>
		</form>
	</div>


	<script>
$(document).ready(function() {
  $('#number').bind("cut copy paste drag drop", function(e) {
      e.preventDefault();
  });     
});
function card(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>

	<script language="Javascript" type="text/javascript">

        function onlyAlphabets(e, t) {
            try {
                if (window.event) {
                    var charCode = window.event.keyCode;
                }
                else if (e) {
                    var charCode = e.which;
                }
                else { return true; }
                if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
                    return true;
                else
                    return false;
            }
            catch (err) {
                alert(err.Description);
            }
        }
        </script >
</body>

</html>