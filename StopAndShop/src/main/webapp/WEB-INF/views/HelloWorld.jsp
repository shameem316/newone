<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Hello World</title>
</head>
<body>
	<h1>${headerMsg}</h1>
	<h1> <i>${name}</i> Welcome to spring mvc application</h1>
	<p> id is ${id} </p>
</body>
</html>