<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title> Website</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="refresh" content="2;URL=#category" >
  <meta content="" name="keywords">
  <meta content="" name="description">

       <!--  =========================== Include HTML script================-->

       

       <!-- ===========================Ends Include HTML script================ -->

       <!-- =========================== Hide Button (View All Categories) script================ -->
       <script>
            $(document).ready(function(){
              $("div").click(function(){
                $(this).hide();
              });
            });
            </script>  

            <script>
            function viewmore() {
              var x = document.getElementById("hidecol1");
              var y = document.getElementById("show1");
               if (x.style.display == "none") {
            
              x.style.display = "block";
               y.style.display="none";
            
              } else {
                x.style.display = "block";
                y.style.display="none";
            
              }
            }
        </script>
            
       <!-- =========================== End Hide Button (View All Categories) script================ -->
 
  <!-- w3school template style -->
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

  <!-- Bootstrap CSS File -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- Main Stylesheet File -->
     <style><%@include file="style-2.css"%></style>
 
  

     <!-- Smooth Scrolling Script File -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <script><%@include file="scrolling_script.js"%></script>
  <script><%@include file="sidebar.js"%></script>

  <style>
  .btnicon{
  margin-top: -55px;
  }
     a {
    color: #03C4EB;
    transition: 0.5s;
  }
  
  a:hover, a:active, a:focus {
    color: #03c5ec;
    outline: none;
    text-decoration: none;
  }
#carticonn {
	margin-top: -70px;
	margin-left: -15px;
	font-size:22px;
	padding-top:-2px;
	
}
#p1{
margin-top: -35px;
	margin-left: -5px;

}

#clearcartt {
	background-color: #222;
	color: white;
	margin-top: -32px;
	margin-left: -5px;
	border: none;
	width: 200px;
}

#mySearch {
	margin-top: 3px;
	margin-left:10px;
	
	height:20px;
	width:300px;
}


#doubleclick {
	display: none;
}

.btn:focus {
	outline: none;
}

ul.navfields{
margin-top:25px;
}
#anchornav{
margin-top:-12px;
}
#buttonnav{
margin-top:1px;
}
  </style>

</head>

<body>

<!--========================================= Navigation Bar =================================-->
    <div  class="nav1">
     <%@ include file="commonnavbar.html" %>
    </div>  

      <!-- Small Screen and Side bar-->
       <div class="snav">
      <%@include file="sidenav.html"%>
</div>
    <!--==========================
 Side Bar Section
  ============================-->

    <div class="nav22">
      <!-- Top menu on small screens -->
   
     
     
     
         <!-- Overlay effect when opening sidebar on small screens -->
         <div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="" id="myOverlay"></div>
     
         <!-- !PAGE CONTENT! -->
         <div class="w3-main" style="margin-left:250px">
     
           <!-- Push down content on small screens -->
           <div class="w3-hide-large" style="margin-top:83px"></div>


           <div> <%@include file="Smallsearch.html"%></div>
    </div>
  <!--End Small Screen and Side bar-->
</div>

<!--========================================= End of Navigation Bar =================================-->



  <!--==========================
  Home Section
  ============================-->
  <section id="home" >
    <div class="home-container">
        <div class="home-logo">
           <img class="" src="/images/homepage/logo2.jpg" alt="Logo"> 
        </div>

        <h1>Welcome to Stop & Shop</h1> 
        <h2> <span class="">Absolutely. Positively. Perfect.
          </span></h2>
        <div class="actions">
          <a href="#category" class="btn-get-started">Get Started</a>
        </div>
    </div>
  </section>

  
  <!--==========================
  Categories Section
  ============================-->
  <section id="category">
    <div class="container "> 
       <br><br><br>

      <!-- ===============================  discount Row ==========================================-->
      
<div class="row-sale">
    <div class="column-sale">
      <img src="/images/homepage/h2.jpg" alt="Snow" style="width:100%"> 
    </div>
    <div class="column-sale">
      <img src="/images/homepage/h9.jpg" alt="Forest" style="width:100%">
    </div>
    <div class="column-sale">
      <img src="/images/homepage/h7.jpg" alt="Mountains" style="width:100%">
    </div>
    <div class="column-sale">
      <img src="/images/homepage/h12.jpg" alt="Mountains" style="width:100%">
    </div>
    <div class="column-sale">
      <img src="/images/homepage/h14.jpg" alt="Mountains" style="width:100%">
    </div>
    <div class="column-sale">
      <img src="/images/homepage/h11.jpg" alt="Mountains" style="width:100%">
    </div>
    <div class="column-sale">
      <img src="/images/homepage/h10.jpg" alt="Mountains" style="width:100%">
    </div>
</div>  <br> 

       <!-- =============================== End of  discount Row ==========================================-->


  <!-- ============================= Automatic Carousel  images/homepage ===================================-->
  <div class="slideshow">
        <img class="mySlides1" src="/images/homepage/h2.jpg"  width="100%" height="200px">
        <img class="mySlides1" src="/images/homepage/h9.jpg"  width="100%" height="200px">
        <img class="mySlides1" src="/images/homepage/h10.jpg"  width="100%" height="200px">
        <img class="mySlides1" src="/images/homepage/h11.jpg"  width="100%" height="200px">      
  </div>

  <script>

        var myIndex = 0;
        carousel();
        
        function carousel() {
          var i;
          var x = document.getElementsByClassName("mySlides1");
          for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";  
          }
          myIndex++;
          if (myIndex > x.length) {myIndex = 1}    
          x[myIndex-1].style.display = "block";  
          setTimeout(carousel, 2000); // Change image every 2 seconds
        }
        
        </script>
        <br>
 <!-- ============================= End Automatic Carousel  images/homepage ===================================-->


      <div class="row" >
        <div class="col-md-12">
          <h3 class="section-title">Categories</h3>
          <div class="section-title-divider"></div> 
          <div class="section-description"><h3><center><strong> Seek. Find. Enjoy </strong> <br> Whatever you've got in mind, we've got inside.</center></h3></div>
          <div class="section-description1"><h4><center><strong> Seek. Find. Enjoy </strong> <br> Whatever you've got in mind, we've got inside.</center></h4></div>
        </div>
      </div>
      
   
      <!--Row 1-->
          <div class="col-md-4 col-sm-6" style="padding-bottom:20px;">
            <a class="category-item" style="background-image: url(/images/homepage/team-3.jpg);text-decoration: none; color:#03C4EB;"> 
            <div class="details">
              <h2><strong>Clothing</strong></h2><span><h4> World Of Fashion </h4></span>
            </div>
            <div class="details-1">
                    <h3><strong>Clothing</strong></h3>
            </div>
          </a>
          <div class="caption"><form action="/subcategory" method="post" style="height:5px;">
      <input type='hidden' id='categoryId' name='categoryId' value='1'/>  
     <strong><center><button type="submit"  style="color:black; background-color:white;border:none;" >Clothing</button></center> </strong></form></div>
        </div>  

        
        <div class="col-md-4 col-sm-6" style="padding-bottom:20px;" >
          <a class="category-item" style="background-image: url(/images/homepage/home_decor1.png); text-decoration: none; color:#03C4EB;" >
            <div class="details">
              <h2><strong> Decor </strong></h2>
              <span><h4> Inspired the Interiors </h4></span>
            </div>
            <div class="details-1">
                    <h3><strong>Decor</strong></h3>
            </div>
          </a>
            <div class="caption"><form action="/subcategory" method="post" style="height:5px;">
      <input type='hidden' id='categoryId' name='categoryId' value='2'/>  
     <strong><center><button type="submit"  style="color:black; background-color:white;border:none;" >Decor</button></center> </strong></form></div>
        </div>

        <div class="col-md-4 col-sm-6" style="padding-bottom:20px;">
          <a class="category-item" style="background-image: url(/images/homepage/access1.jpg);text-decoration: none; color:#03C4EB;" >
            <div class="details">
              <h2><strong> Accessories </strong></h2>
              <span><h4>Life is short, make every outfit count </h4></span>
            </div>
            <div class="details-1">
                    <h3><strong>Accessories</strong></h3>
            </div>
          </a>
            <div class="caption"><form action="/subcategory" method="post" style="height:5px;">
      <input type='hidden' id='categoryId' name='categoryId' value='3'/>  
       <strong><center><button type="submit"  style="color:black; background-color:white;border:none;" >Accessories</button></center> </strong></form></div>
        </div>
    <!--End Row 1-->  
    

  
    <!-- Row 2 -->
        <div class="col-md-4 col-sm-6" style="padding-bottom:20px;" >
          <a class="category-item" style="background-image: url(/images/homepage/shoe1.jpg);text-decoration: none; color:#03C4EB;" >
            <div class="details">
              <h2><strong> Shoes </strong></h2>
              <span><h4>Equip Your Feet </h4></span>
            </div>
            <div class="details-1">
                    <h3><strong>Shoes</strong></h3>
            </div>
          </a>
          <div class="caption"><form action="/subcategory" method="post" style="height:5px;">
      <input type='hidden' id='categoryId' name='categoryId' value='4'/>  
       <strong><center><button type="submit"  style="color:black; background-color:white;border:none;" >Shoes</button></center> </strong></form></div>
        </div> 

        <center> <div><a onclick="viewmore()"  id="show1" style="text-decoration: none; color:grey;"> All Categories </a></div></center>

<div id= "hidecol1">

        <div class="col-md-4 col-sm-6" style="padding-bottom:20px;">
          <a class="category-item" style="background-image: url(/images/homepage/toys1.jpg);text-decoration: none; color:#03C4EB;" >
            <div class="details">
              <h2><strong> Toys </strong></h2>
              <span><h4> A Child's Fantasty world </h4></span>
            </div>
            <div class="details-1">
                    <h3><strong>Toys</strong></h3>
            </div>
          </a>
           <div class="caption"><form action="/subcategory" method="post" style="height:5px;">
      <input type='hidden' id='categoryId' name='categoryId' value='5'/>  
       <strong><center><button type="submit"  style="color:black; background-color:white;border:none;" >Toys</button></center> </strong></form></div>
        </div>

        <div class="col-md-4 col-sm-6" style="padding-bottom:20px;" >
          <a class="category-item" style="background-image: url(/images/homepage/books1.jpg);text-decoration: none; color:#03C4EB;">
            <div class="details">
              <h2><strong> Books </strong></h2>
              <span><h4> What goes into the mind comes out in a life </h4></span>
            </div>
            <div class="details-1">
                    <h3><strong>Books</strong></h3>
            </div>
          </a>
           <div class="caption"><form action="/subcategory" method="post" style="height:5px;">
      <input type='hidden' id='categoryId' name='categoryId' value='6'/>  
       <strong><center><button type="submit"  style="color:black; background-color:white;border:none;" >Books</button></center> </strong></form></div>
        </div>
</div> <!-- Hide these 2 columns -->

  <!-- End Row 2 -->
      
     </div> 
  </section>

  <!-- End of Category Section-->

   <!--==========================
  About Section
  ============================-->
<!--   <section id="about">
  
  
</section> -->
<center><div class="about" style="width:60%;"><%@ include file="about_us_new.html" %></div></center>
  <!-- ==========================
  Contact Section
  ============================-->
  <section id="contact">
   <%@include file="contact_us_new1.html"%>
</section>

  <!--==========================
  Footer
  ============================-->
 
  <footer id="footer">
      <%@include file="footer.html"%>
  </footer>  
  <!-- #footer -->




</body>
</html>

























