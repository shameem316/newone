<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
  <style><%@ include file="font.css" %></style> 
  <style><%@ include file="cSS.css" %></style> 
  <style><%@ include file="my.css" %></style> 
<style><%@ include file="boot.css" %></style> 

 <%@ include file="commonnavbar.html" %>
<div style="margin-left:-350px;"> <%@ include file="sidenav.html" %></div>

 <script> <%@ include file="sidebar.js" %></script>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="animate.css">
<link rel="stylesheet" href="footer.css">
<link rel="stylesheet" href="contact_us.css">
<link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet'>   <!-- to use google fonts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="C:\Users\172949\Desktop\p\addtocart.js"></script>
<script src="sidebar.js"></script>
<title>SignUp</title>
<style>
#carticonn {
	margin-top: -2px;
	margin-left: 10px;
	font-size:22px;
	
}
#p1{
margin-top: -25px;
	margin-left: -5px;

}

#clearcartt {
	background-color: #222;
	color: white;
	margin-top: -32px;
	margin-left: -5px;
	border: none;
	width: 200px;
}

#mySearch {
	margin-top: 3px;
	margin-left:10px;
	
	height:20px;
	width:300px;
}


#doubleclick {
	display: none;
}

.btn:focus {
	outline: none;
}

ul.navfields{
margin-top:20px;
}
#anchornav{
margin-top:-9px;
}
#buttonnav{
margin-top:3px;
}
input[type=password] {
  width: 50%;
  margin-bottom: 20px;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 3px;
}
.btn1 {
  background-color: none;
  color: black;
  padding: 12px;
  margin: 10px 0;
  border: none;
  width: 25%;
margin-right:25px;
margin-left:28px;
  border-radius: 3px;
  cursor: pointer;
  font-size: 17px;
}

input[type=text] {
  width: 50%;
  margin-bottom: 20px;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 3px;
}

</style>
</head>

<body class="w3-content w1-bgcolor" style="" id="b">

<div>
<br><br><br><br><br>
</div>
<div class="col-75" align="center" style="vertical-align: middle">
        
          <div class="col-50" align="center" style="vertical-align: middle">
          
          <center><h3><b><font color="black" size="6">Sign Up</font></b></h3></center>
      <div class="section-title-divider"></div>
      <center><h4><font color="green">${msgSignup}</font> </h4></center>
      <center><h4><font color="green">${msgfailSignup}</font> </h4></center>
	<h4><font color="black" size="4">A few details and you're on your way ...</font></h4></center>
	
            </div>
			 <form:errors path="signupRQ.*"></form:errors> 
			<form action="/create-account" method="post">
			
			<div>
          <%--  <label  for="username"><spring:message  code="label.username"></spring:message></label> --%>
						<input type="text" name="username" placeholder="Enter UserName"	  required="true" style="width:50%" />
						</div>
						<div>
            <%-- <label  for="firstname"><spring:message  code="label.firstname"></spring:message></label>  --%>
						<input type="text" name="firstname" placeholder="Enter First Name"	onkeypress="return onlyAlphabets(event,this);" required="true" style="width:50%" />
						</div>
						<div>
			 <%-- <label  for="lastname"><spring:message  code="label.lastname"></spring:message></label> --%>
						<input type="text" name="lastname" placeholder="Enter Last Name"	 onkeypress="return onlyAlphabets(event,this);" required="true" style="width:50%" />
						</div>
						<div>
			<%-- <label  for="email"><spring:message  code="label.email"></spring:message></label> --%>
						<input type="text" name="email" placeholder="Enter Email"	required="true" style="width:50%" />
						</div>
						
			<%-- <label  for="phone"><spring:message  code="label.phone"></spring:message></label> --%>
						<input type="text" name="phone" placeholder="Enter Phone" maxlength="10"	 onkeypress="return isNumberKey(event)" required="true" style="width:50%"/>
						
						<div>
			<%-- <label  for="password"><spring:message  code="label.password"></spring:message></label> --%>
						<input type="password" name="password" placeholder="Enter Password"	required="true"/>
						</div>
	
          </div>
          
        
       <div>
         <center><button  type="submit" class="btn1">SignUp </button> </center>
      
      </div>
	
      </form>
      
     
<br>
<br>
<%@ include file="about_us.html" %>
<%@include file="Smallsearch.html" %>
<%@include file="contact_us.html" %>
<%@ include file="footer.html" %>
<script> <%@ include file="script.js" %></script>
<script><%@ include file="scripjq.js" %></script>
</body>

<script>
$(document).ready(function() {
  $('#number').bind("cut copy paste drag drop", function(e) {
      e.preventDefault();
  });     
});
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>

<script language="Javascript" type="text/javascript">

        function onlyAlphabets(e, t) {
            try {
                if (window.event) {
                    var charCode = window.event.keyCode;
                }
                else if (e) {
                    var charCode = e.which;
                }
                else { return true; }
                if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
                    return true;
                else
                    return false;
            }
            catch (err) {
                alert(err.Description);
            }
        }

    </script>
</html>