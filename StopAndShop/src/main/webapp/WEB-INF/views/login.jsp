<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<style><%@ include file="font.css" %></style> 
  <style><%@ include file="cSS.css" %></style> 
  <style><%@ include file="my.css" %></style> 
<style><%@ include file="boot.css" %></style> 
<style><%@ include file="navbar.css" %></style> 
 
<%@ include file="commonnavbar.html" %>
<div style="margin-left:100px;"> <%@ include file="sidenav.html" %></div>

 <script> <%@ include file="sidebar.js" %></script>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
</head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="animate.css">
<link rel="stylesheet" href="footer.css">
<link rel="stylesheet" href="contact_us.css">
<link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet'>   <!-- to use google fonts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="sidebar.js"></script>
<style>
body {
  font-family: Arial;
  font-size: 17px;
  padding: 8px;
}

* {
  box-sizing: border-box;
}

.container {
  background-color: white;
  padding: 5px 20px 15px 20px;
 

}
input[type=password] {
  width: 50%;
  margin-bottom: 20px;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 3px;
}
.text1 {
  width: 50%;
  margin-bottom: 20px;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 3px;
}



label {
  margin-bottom: 10px;
  display: block;
}


.btn1 {
  background-color: none;
  color: black;
  padding: 12px;
  margin: 10px 0;
  border: none;
  width: 25%;
margin-right:25px;
margin-left:28px;
  border-radius: 3px;
  cursor: pointer;
  font-size: 17px;
}


@media (max-width: 800px) {
 
.smallhide{
display:none;
margin-left:300px;
}
.containermain{
margin-left:0px;
}
@media(min-width:800px){
div.containermain{

margin-left:300px;
}
div.smallhide{
display:block;
}
}
.containermain{
margin-left:0px;
}

/* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other (also change the direction - make the "cart" column go on top) */
@media (max-width: 800px) {
  .row {
    flex-direction: column-reverse;
  }
 
}

.section-title-divider {
    width: 50px;
    height: 3px;
    background: #03C4EB;
    margin: 0 auto;
    margin-bottom: 20px;
  }

#carticonn {
	margin-top: -25px;
	margin-left: -15px;
	font-size:22px;
	
}
#p1{
margin-top: -25px;
	margin-left: -5px;

}

#clearcartt {
	background-color: #222;
	color: white;
	margin-top: -32px;
	margin-left: -5px;
	border: none;
	width: 200px;
}

#mySearch {
	margin-top: 3px;
	margin-left:10px;
	
	height:20px;
	width:300px;
}


#doubleclick {
	display: none;
}

.btn:focus {
	outline: none;
}

ul.navfields{
margin-top:10px;
}
#anchornav{
margin-top:-9px;
}
#buttonnav{
margin-top:3px;
}
</style>
<body class=" w1-bgcolor" style="" id="b">
     <div class="containermain">

<div>
<br><br><br><br><br>
</div>


<h1>${msg}</h1>
   <div class="container">


  <div class="col-75" align="center" style="vertical-align: middle">
        
          <div class="col-50" align="center" style="vertical-align: middle">
          
          <center><h3><b><font color="black" size="6">Login</font></b></h3>
      <div class="section-title-divider"></div>
		<h4><font color="black" size="4">Well, Hello there..!!</font></h4></center>
            </div>
			
			<form action="/login" method="post">
			
			<fieldset>
				<input placeholder="Enter Username" id="username" class="text1" name="username" " required="true">
			</fieldset>
			<fieldset>
				<input placeholder="Enter Password " type="password" id="password"	name="password" required="true">  <br />
			</fieldset>
        
       <div>
        <center><button  type="submit" class="btn1">Login </button> </center>
      </div>
	  
      </form>
    
 <br>
<br>
<div class="smallhide">
<%@ include file="about_us.html" %>
</div>
<div class="smallhide">
<%@ include file="contact_us.html" %>
</div>
<div class="">
<%@ include file="footer.html" %>
</div>
 
    <script> <%@ include file="script.js" %></script>
     <script><%@ include file="scripjq.js" %></script>
     </div>
     </div>
</body>
  





<script language="Javascript" type="text/javascript">

        function onlyAlphabets(e, t) {
            try {
                if (window.event) {
                    var charCode = window.event.keyCode;
                }
                else if (e) {
                    var charCode = e.which;
                }
                else { return true; }
                if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
                    return true;
                else
                    return false;
            }
            catch (err) {
                alert(err.Description);
            }
        }

    </script>

</body>
</html>