package com.nttdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
public class StopAndShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(StopAndShopApplication.class, args);
	}

}
                                                                     