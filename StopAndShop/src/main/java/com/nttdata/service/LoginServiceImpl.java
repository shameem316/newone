package com.nttdata.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.nttdata.dao.LoginDAO;
import com.nttdata.request.LoginRQ;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	private LoginDAO logindao;
		
	@Override
	public int authenticate(LoginRQ loginRQ)  {

		int result = logindao.authenticateDAO(loginRQ);
		return result;
	}

}
