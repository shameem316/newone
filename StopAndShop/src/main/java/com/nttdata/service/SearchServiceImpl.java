package com.nttdata.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;





import com.nttdata.dao.SearchDAOImpl;
import com.nttdata.dao.SearchDao;
import com.nttdata.request.SearchRQ1;

@Service
public class SearchServiceImpl implements SearchService {

	
	@Autowired
	SearchDao searchDao;
	
	@Override
	public List searchResult(SearchRQ1 searchRQ) {
		
		List result=null;
		
			result = searchDao.searchResult(searchRQ);
			
		 
		
		
		if(result==null)
		{
			return null;
		}
		
		else
			return result;

}
}
