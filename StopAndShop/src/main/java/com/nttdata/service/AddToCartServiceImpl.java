package com.nttdata.service;

import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.stereotype.Service;

import com.nttdata.dao.AddToCartDao;
import com.nttdata.request.CartRQ;


@Service
public class AddToCartServiceImpl implements AddToCartService {
	
	
	boolean result=true;

	@Autowired
	AddToCartDao cart_Obj;
	@Override
	public boolean addtocart(CartRQ cartRQ) {
		result=cart_Obj.addtocart(cartRQ);
		return true;
	}
	@Override
	 public boolean clearcart(CartRQ cartRQ) {
		result=cart_Obj.clearcart(cartRQ);
		return true;
		
	}
	
	
	

}
