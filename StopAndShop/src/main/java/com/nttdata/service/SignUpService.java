package com.nttdata.service;
import com.nttdata.request.SignUpRQ;

public interface SignUpService 
{
	public int signUpService(SignUpRQ signUpRQ);
}
