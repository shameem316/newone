package com.nttdata.service;

import com.nttdata.request.PaymentGatewayRQ;
@FunctionalInterface
public interface PaymentGatewayService {
public boolean pay(PaymentGatewayRQ p); 

}
