package com.nttdata.service;

import com.nttdata.request.LoginRQ;

public interface LoginService {

	public int authenticate(LoginRQ loginRQ) ;

}
