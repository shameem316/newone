package com.nttdata.service;

import java.sql.ResultSet;
import java.util.ArrayList;

import com.nttdata.request.DisplayCartRQ;

public interface DisplayCartService {
	public ArrayList validateDisplayCart() throws Exception;

}
