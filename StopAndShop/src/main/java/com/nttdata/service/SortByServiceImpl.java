package com.nttdata.service;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nttdata.dao.SortByDAO;

@Service
@Transactional
public class SortByServiceImpl implements SortByService {

	@Autowired
	SortByDAO sortByDAO;
	@Autowired
	HttpServletRequest request;
	@Autowired
	SessionFactory sf;
	
	@Override
	public ArrayList sortByNewest(String category_id)  {
		
		ArrayList list=new ArrayList();
		 
		
		try {
			list=sortByDAO.sortByNewest(category_id);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return list;
	
	
	}

	@Override
	public ArrayList sortByPrice0to1000(String category_id) throws Exception {
		ArrayList list=new ArrayList();
		
		try {
			list=sortByDAO.sortByPrice0to1000(category_id);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return list;
	}
	
	@Override
	public ArrayList sortByPrice1001to2000(String category_id) throws Exception {
		ArrayList list=new ArrayList();
		
		try {
			list=sortByDAO.sortByPrice1001to2000(category_id);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return list;
	}
	@Override
	public ArrayList sortByPrice2000above(String category_id) throws Exception {
		ArrayList list=new ArrayList();
		
		try {
			list=sortByDAO.sortByPrice2000above(category_id);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return list;
	}
	
	
	}


