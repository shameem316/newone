package com.nttdata.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nttdata.dao.PaymentGatewayDao;
import com.nttdata.request.PaymentGatewayRQ;

@Service
public class PaymentGatewayServiceImpl implements PaymentGatewayService {
	boolean result;
	@Autowired
	PaymentGatewayDao PDAO;
	
	@Override
	public boolean pay(PaymentGatewayRQ p) {
		
		result= PDAO.payDao(p);
		return result;	
		
	}
	

}
