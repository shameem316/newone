package com.nttdata.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;
@Service
public interface SortByService {
	public ArrayList sortByNewest(String category_id) throws Exception;
	
	public ArrayList sortByPrice0to1000(String category_id) throws Exception;
	
	public ArrayList sortByPrice1001to2000(String category_id) throws Exception;
	
	public ArrayList sortByPrice2000above(String category_id) throws Exception;

}
