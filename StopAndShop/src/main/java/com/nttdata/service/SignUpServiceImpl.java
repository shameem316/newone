package com.nttdata.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.nttdata.dao.SignUpDAO;
import com.nttdata.model.SignUp;
import com.nttdata.request.SignUpRQ;
import com.nttdata.util.CommonUtil;

@Service
public class SignUpServiceImpl implements SignUpService {


	@Autowired
	SignUpDAO signUp;

	@Override
	public int signUpService(SignUpRQ s)  {
		int result=0;
		SignUp signup= CommonUtil.signUp(s);
		result = signUp.signUpDAO(signup);
		return result;

	}

}
