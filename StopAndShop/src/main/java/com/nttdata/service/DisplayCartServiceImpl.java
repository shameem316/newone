package com.nttdata.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nttdata.dao.DisplayCartDAO;
import com.nttdata.request.DisplayCartRQ;
@Service
public class DisplayCartServiceImpl implements DisplayCartService {

	@Autowired
	DisplayCartDAO DisplayDAO;
	
	@Override
	public ArrayList validateDisplayCart() throws Exception {
		ArrayList list=new ArrayList();
		
		try {
			list=DisplayDAO.displayCart();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return list;
	
	
	}
	}


