package com.nttdata.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.nttdata.dao.ContactUsDAO;
import com.nttdata.model.ContactUs;
import com.nttdata.request.ContactUsRQ;
import com.nttdata.util.CommonUtil;

@Service
public class ContactUsServiceImpl implements ContactUsService 
{
	@Autowired
	ContactUsDAO contact;

	@Override
	public  int contactService(ContactUsRQ r) 
	{
		int result=0;
		ContactUs contactus = CommonUtil.contactUs(r);
		result= contact.contactDAO(contactus);
		return result;
	}
  
}
