package com.nttdata.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.HibernateException;
import org.omg.CORBA.COMM_FAILURE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import com.nttdata.request.LoginRQ;
import com.nttdata.service.LoginService;
import com.nttdata.util.CommonConstants;

@Controller
public class LoginController {

	@Autowired
	private LoginService loginService;

	@Autowired
	private HttpServletRequest request;
	
	@PostMapping("/loginDemo")
	public String login()
	{
		return CommonConstants.loginPage;
	}
 
	@PostMapping("/login")
	public String authenticate(@ModelAttribute LoginRQ loginRQ, Model model)  {
		
		int userId = loginService.authenticate(loginRQ);

		System.out.println("in controller:"+userId );
		if (userId>0) 
		{
			HttpSession session=request.getSession(false);
			session.setAttribute(CommonConstants.userIdSession, userId);
			return CommonConstants.homePage;
		} 
		else 
		{
			model.addAttribute(CommonConstants.loginFailureModel, CommonConstants.loginFailure);
			return CommonConstants.loginPage;
		}

	}

	@ModelAttribute
	public void getHeaderMessage(Model model) {
		model.addAttribute(CommonConstants.welcomeMsgModel, CommonConstants.welcomeMsg);
	}

	@ExceptionHandler(NullPointerException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public String handleNullPointerExp(Model model) {
		model.addAttribute(CommonConstants.failureModel, CommonConstants.failureMsg);
		return CommonConstants.failurePage;
	}
	
		@ExceptionHandler(HibernateException.class)
		@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
		public String handleHibernateExp(Model model) {
			model.addAttribute(CommonConstants.failureModel, CommonConstants.failureHibernateMsg);
			return CommonConstants.failurePage;
	}


}
