package com.nttdata.controller;
import javax.validation.Valid;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import com.nttdata.request.ContactUsRQ;
import com.nttdata.service.ContactUsService;
import com.nttdata.util.CommonConstants;

@Controller
public class ContactUsController {

	@Autowired
	private ContactUsService contactService;

	@PostMapping("/contact_us")
	public String contactUs(@Valid @ModelAttribute ContactUsRQ contact, BindingResult result,  Model model) 
	{
		int res = contactService.contactService(contact);
		if (result.hasErrors()) {
			System.out.println("Errors Executed ");
			return CommonConstants.contactUsPage;
		}
		if(res>0)
		{
		model.addAttribute(CommonConstants.contactSuccessModel, CommonConstants.contactSuccess);
		return CommonConstants.homePage;
		}
		else
		{
			model.addAttribute(CommonConstants.contactFailureModel,CommonConstants.contactFailure);
			return CommonConstants.homePage;
		}
	}

	@ModelAttribute
	public void getHeaderMessage(Model model) {
		model.addAttribute(CommonConstants.welcomeMsgModel, CommonConstants.welcomeMsg);
	}

	@ExceptionHandler(NullPointerException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public String handleNullPointerExp(Model model) {
		model.addAttribute(CommonConstants.failureModel, CommonConstants.failureMsg);
		return CommonConstants.failurePage;
	}

	@ExceptionHandler(HibernateException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public String handleHibernateExp(Model model) {
		model.addAttribute(CommonConstants.failureModel, CommonConstants.failureHibernateMsg);
		return CommonConstants.failurePage;
	}
}
