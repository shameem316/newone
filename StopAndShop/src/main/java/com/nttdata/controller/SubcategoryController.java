package com.nttdata.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import com.nttdata.util.CommonConstants;



@Controller
public class SubcategoryController {
	@Autowired
	private HttpServletRequest request;
	
	public static final Map<String, String> map = new HashMap<>();
	{
		map.put("1", "clothing");
		map.put("2", "Decor");
		map.put("3", "Accessories");
		map.put("4", "Shoes");
		map.put("5", "toys");
		map.put("6", "books");
	}

	@PostMapping("/subcategory")
	public String create() {
		String cid=request.getParameter("categoryId");
		
		return map.get(cid);
	}

}
