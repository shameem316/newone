package com.nttdata.controller;
import javax.validation.Valid;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import com.nttdata.request.SignUpRQ;
import com.nttdata.service.SignUpService;
import com.nttdata.util.CommonConstants;

@Controller
public class SignUpController {
	

	@Autowired
	private SignUpService signUpService;

	@PostMapping("/create-accountDemo")
	public String create() {
		
		return CommonConstants.createAccountPage;
	}

	@PostMapping("/create-account")
	public String createAccount1(@Valid @ModelAttribute SignUpRQ signUpRQ, BindingResult result, Model model)  {
		int res = signUpService.signUpService(signUpRQ);

		if(res>0)
		{
		model.addAttribute(CommonConstants.signUpSuccessModel, CommonConstants.signUpSuccess);
		return CommonConstants.createAccountPage;
		}
		else
		{
			model.addAttribute(CommonConstants.signUpFailureModel,  CommonConstants.signUpFailure);
			return CommonConstants.createAccountPage;
		}
	}

	
	@ModelAttribute
	public void getHeaderMessage(Model model) {
		model.addAttribute(CommonConstants.welcomeMsgModel, CommonConstants.welcomeMsg);
	}

	@ExceptionHandler(NullPointerException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public String handleNullPointerExp(Model model) {
		model.addAttribute(CommonConstants.failureModel, CommonConstants.failureMsg);
		return CommonConstants.failurePage;
	}

	@ExceptionHandler(HibernateException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public String handleHibernateExp(Model model) {
		model.addAttribute(CommonConstants.failureModel, CommonConstants.failureHibernateMsg);
		return CommonConstants.failurePage;
	}
}
