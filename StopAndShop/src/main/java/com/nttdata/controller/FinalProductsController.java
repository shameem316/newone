package com.nttdata.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.nttdata.model.FormalShoe;
import com.nttdata.service.FinalProuctService;

@Controller
public class FinalProductsController {

	@Autowired
	private FinalProuctService finalProuctService;


	@PostMapping("/finalProducts")
	public String formalShoeRQs(@Valid @ModelAttribute FormalShoe formalShoe, Model model, BindingResult result,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		System.out.println("--------------In final products controller--------------");
		
		String categId = request.getParameter("categoryId");
		HttpSession session = request.getSession(false);
		session.setAttribute("category_id", categId);
		session.setAttribute("subcategory_id", "1");

		String category_id = (String) session.getAttribute("category_id");

		ArrayList list = finalProuctService.productListing(category_id);

		if (list.size() > 0) {
			model.addAttribute("list", list);
			model.addAttribute("listSize", list.size());
			return "finalProducts";
		} else {
			return "failure";
		}

	}
}

