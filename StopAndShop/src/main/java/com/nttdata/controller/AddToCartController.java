package com.nttdata.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;

import com.nttdata.request.CartRQ;
import com.nttdata.service.AddToCartService;
import com.nttdata.service.FinalProuctService;
import com.nttdata.util.PrototypeBeans;

@Controller
public class AddToCartController {

	@Autowired
	private AddToCartService addtocartservice;
	@Autowired
	private HttpServletRequest request;
	@Autowired
	private PrototypeBeans beans;
	@Autowired
	public CartRQ cartRQ;

	@Autowired
	private FinalProuctService finalProuctService;
	
	
	public static final Map<String, String> map = new HashMap<>();
	{
		map.put("1", "finalProducts");
		map.put("2", "finalProducts");
		map.put("3", "finalProducts");
		map.put("4", "finalProducts");
		map.put("5", "finalProducts");
		map.put("6", "finalProducts");
	}

	@PostMapping("/addtocart")
	public String cart(Model model) {
		 try { 

			System.out.println("Controller***********************add to cart");
			HttpSession session = request.getSession(false);
			/* session.setAttribute("userId",1); */
			System.out.println("in session");
			 session.getAttribute("userId"); 
		

			int user_id = (int) session.getAttribute("userId");
			
		/*
		 * String user=String.valueOf(user_id);
		 */
			String category_id = (String) request.getParameter("categoryId");
		
			String subcategory_id = (String) request.getParameter("subcategoryId");
			String product_id = (String) request.getParameter("productId");
			
			String price = (String) request.getParameter("price");
			session.setAttribute("price", price);
			String brand = (String) request.getParameter("brand");
			session.setAttribute("brand", brand);

			cartRQ.setUser_id(user_id);
			cartRQ.setCategory_id(category_id);
			cartRQ.setSubcategory_id(subcategory_id);
			cartRQ.setProduct_id(product_id);

			 if (cartRQ.getUser_id() != 0)  {

				addtocartservice.addtocart(cartRQ);

			 } 
		
			
			String categoryId = cartRQ.getCategory_id();
			System.out.println("cat+++++++++" + categoryId);
			ArrayList list = finalProuctService.productListing(categoryId);
			 model.addAttribute("list", list);
			model.addAttribute("listSize", list.size());
			 
			  model.addAttribute("list", list); model.addAttribute("listSize",list.size()); 
			return (map.get(categoryId));
	} 
		  catch (NullPointerException e) {
		  
		  System.out.println("******************************in catchhhhh ");
		  
		  return "login";
		  }
		 

	 } 

	@PostMapping("/clearcart")
	public String cartclear(CartRQ cartRQ) {

		HttpSession session = request.getSession(false);

		session.getAttribute("userId");

		int user_Id = (int) session.getAttribute("userId");

		cartRQ.setUser_id(user_Id);
		addtocartservice.clearcart(cartRQ);

		return "EmptyCart";

	}

}
//  <input type='hidden' id='categoryId' name='categoryId' value='${list.category_id}'/> 