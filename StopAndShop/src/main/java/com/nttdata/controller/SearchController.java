package com.nttdata.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.nttdata.request.SearchRQ1;
import com.nttdata.service.SearchService;


@Controller
public class SearchController {

	@Autowired
	private SearchService SearchService;
	
	

	@PostMapping("/search")
	
	public String createAccount(@ModelAttribute SearchRQ1 searchRQ, BindingResult result, Model model) {
		System.out.println("Controller***********************");
		List list = SearchService.searchResult(searchRQ);
          
		System.out.println(list);
		
		
		if(list==null) {
			model.addAttribute("msg","no products found");
			return "SearchError";
			
		}
		else if (list.size() > 0) {
			model.addAttribute("list", list);
			model.addAttribute("listSize", list.size());
			return "finalProducts";
		}
		else if(list.size()==0) {
			model.addAttribute("msg","no products found");
			return "SearchError";
		}
		
		else return null;

	}
	

	

}
