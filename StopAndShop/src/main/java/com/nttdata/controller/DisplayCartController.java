package com.nttdata.controller;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;

import com.nttdata.service.DisplayCartService;
import com.nttdata.service.FinalProuctService;

@Controller
public class DisplayCartController {

	@Autowired
	private DisplayCartService displaycartService;
	
	@Autowired
	private FinalProuctService finalProuctService;
	

	@PostMapping("/displaycart")
	  public String display_Cart(@Valid Model model) throws Exception
	  {
		  
			ArrayList list=displaycartService.validateDisplayCart();
			System.out.println("----------------- in display cart controller------size------------"+list.size());
	
			if(list.size()>0)		 
			{
				model.addAttribute("list", list);
				model.addAttribute("listSize", list.size());
				return "displaycart";
			}
			else
			{
				return "EmptyCart";
			}
	 
	  }
	
}


