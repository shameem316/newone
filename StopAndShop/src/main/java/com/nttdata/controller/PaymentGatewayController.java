package com.nttdata.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.nttdata.request.PaymentGatewayRQ;
import com.nttdata.service.PaymentGatewayService;

@Controller

public class PaymentGatewayController {
	@Autowired
	private PaymentGatewayService paymentService;

	/*@Autowired 

private PaymentGatewayRQ paymentgatewayRQ;*/
	@Autowired
	public HttpServletRequest request;

	@GetMapping("/payment-demo")
	public String homePage() {
		System.out.println("*********Home Page************************");
		return "payment";
	}

	@PostMapping("/payment")
	public String payment(@Valid @ModelAttribute PaymentGatewayRQ paymentRQ, BindingResult result, Model model) {
		System.out.println("Controller***********************");
		
			  
		  paymentService.pay(paymentRQ);
		  model.addAttribute("msg", "Payment Successfull");
	

		if (result.hasErrors()) {
			System.out.println("Errors Executed ");
			return "failure";
		}
		
		model.addAttribute("PaymentGatewayRQ", paymentRQ);
		return "PaymentSuccess";
	}
	
	
}
