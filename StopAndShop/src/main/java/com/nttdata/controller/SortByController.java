package com.nttdata.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.nttdata.model.FormalShoe;
import com.nttdata.service.SortByService;

@Controller
public class SortByController {

	@Autowired
	private SortByService sortByService;
	@Autowired
	private HttpServletRequest request;
	
	

	@PostMapping("/newest")
	public String sortByNewest(@Valid @ModelAttribute FormalShoe formalShoe, Model model, BindingResult result,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println("*************************In newest Shoes Controller***********************");
		HttpSession session = request.getSession(false);
		String category_id = (String) session.getAttribute("category_id");
		ArrayList list = sortByService.sortByNewest(category_id );
		System.out.println("---------------in newest controller-----------------");
		

		if (list.size() > 0) {
			model.addAttribute("list", list);
			model.addAttribute("listSize", list.size());
			return "finalProducts";
		} else {
			return "failure";
		}
	}
	
	  @PostMapping("/price0to1000")
	  public String sortByPrice0to1000(@Valid @ModelAttribute FormalShoe formalShoe,Model model, BindingResult result ,HttpServletRequest request,HttpServletResponse response) throws Exception
	  {
	 System.out.println("*************************In newest Shoes Controller***********************");
	 HttpSession session = request.getSession(false);
		String category_id = (String) session.getAttribute("category_id");
	 ArrayList list=sortByService.sortByPrice0to1000(category_id);
	 System.out.println("---------------in newest controller-----------------"+list.size());
	
	
	
	 if(list.size()>0)		 
	 {
		 model.addAttribute("list", list);
		 model.addAttribute("listSize", list.size());
		 return "finalProducts";
	 }
	 else
	 {
		 return "failure";
	 }
	 	
	  }
	  
	  
	  @PostMapping("/price1001to2000")
	  public String sortByPrice1001to2000(@Valid @ModelAttribute FormalShoe formalShoe,Model model, BindingResult result ,HttpServletRequest request,HttpServletResponse response) throws Exception
	  {
	 System.out.println("*************************In newest Shoes Controller***********************");
	 HttpSession session = request.getSession(false);
		String category_id = (String) session.getAttribute("category_id");
	 ArrayList list=sortByService.sortByPrice1001to2000(category_id);
	 System.out.println("---------------in newest controller-----------------");
	
		
	 if(list.size()>0)		 
	 {
		 model.addAttribute("list", list);
		 model.addAttribute("listSize", list.size());
		 return "finalProducts";
	 }
	 else
	 {
		 return "failure";
	 }
	 	 
	  }
	 
	  @PostMapping("/price2000above")
	  public String sortByPrice2000above(@Valid @ModelAttribute FormalShoe formalShoe,Model model, BindingResult result ,HttpServletRequest request,HttpServletResponse response) throws Exception
	  {
	 System.out.println("*************************In newest Shoes Controller***********************");
	 HttpSession session = request.getSession(false);
		String category_id = (String) session.getAttribute("category_id");
	 ArrayList list=sortByService.sortByPrice2000above(category_id);
	 System.out.println("---------------in newest controller-----------------");
	
	
	
	 if(list.size()>0)		 
	 {
		 model.addAttribute("list", list);
		 model.addAttribute("listSize", list.size());
		 return "finalProducts";
	 }
	 else
	 {
		 return "failure";
	 }
	 
	 
	  }

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

// <Context docBase="C:/Users/172981/Desktop/data" path="/formal-shoes"/>