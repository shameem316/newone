package com.nttdata.util;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.nttdata.model.ContactUs;
import com.nttdata.model.SignUp;
import com.nttdata.request.ContactUsRQ;
import com.nttdata.request.FinalProductRQ;
import com.nttdata.request.SignUpRQ;
	
	public interface CommonUtil {
		
		
		@Autowired
		PrototypeBeans prototypeBeans=null;
		
		public static String table[] ={ "", "Jeans", "Clocks", "Watches", "FormalShoe", "SoftToys", "FictionalBooks" };
		
		
		
		public static String enCode(String password) {
			String encode = null;
			try {
				//Encode
				encode = Base64.getEncoder().encodeToString(password.getBytes());
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println(encode); 
			return encode;
		}

	
		public static SignUp signUp(SignUpRQ signUpRQ) 
		{
			SignUp signup= new SignUp(signUpRQ.getUsername(), signUpRQ.getFirstname(), signUpRQ.getLastname(), signUpRQ.getEmail(), CommonUtil.enCode(signUpRQ.getPassword()), signUpRQ.getPhone());
			return  signup;
		}

		public static ContactUs contactUs(ContactUsRQ contactUsRQ) 
		{
			ContactUs contactus= new ContactUs(contactUsRQ.getName(), contactUsRQ.getEmail(), contactUsRQ.getPhone(), contactUsRQ.getSubject(), contactUsRQ.getMsg());
				return  contactus;
		}
		
		public static String getTableName(int cid) {
			
			return table[cid];
		}

		public static List getProducts(FinalProductRQ finalProductRQ, ArrayList list, Object obj) {
			
			Object o[] = (Object[]) obj;
			
			FinalProductRQ fs = finalProductRQ;
			fs.setProduct_id((String) o[0]);
			fs.setBrand((String) o[1]);
			fs.setCategory_id((String) o[2]);
			fs.setDescription((String) o[3]);
			fs.setNewest((String) o[4]);
			fs.setPopularity((String) o[5]);
			fs.setPrice((String) o[6]);
			fs.setSubcategory_id((String) o[7]);

			list.add(fs);
			return list;
		}
	}
