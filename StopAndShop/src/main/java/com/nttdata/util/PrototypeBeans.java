package com.nttdata.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

import com.nttdata.request.CartRQ;
import com.nttdata.request.Details;
import com.nttdata.request.DisplayCartRQ;
import com.nttdata.request.FinalProductRQ;


@Component
public class PrototypeBeans {

	


	
	@Autowired
	private CartRQ cartRQ;
	
	@Autowired
	private DisplayCartRQ disp_cart;
	
	@Autowired
	private Details details;
	
	@Autowired
	private FinalProductRQ finalProductRQ;
    
	
	@Lookup
	public FinalProductRQ getFinalProductRQ() {
		return null;
	}

	public void setFinalProductRQ(FinalProductRQ finalProductRQ) {
		this.finalProductRQ = finalProductRQ;
	}
	
	

	@Lookup
	public CartRQ getCartRQ() {
		return null;
	}

	public void setCartRQ(CartRQ cartRQ) {
		this.cartRQ = cartRQ;
	}
	

	@Lookup
	public DisplayCartRQ getDisp_cart() {
		return null;
	}

	public void setDisp_cart(DisplayCartRQ disp_cart) {
		this.disp_cart = disp_cart;
	}

	@Lookup
	public Details getDetails() {
		return null;
	}

	public void setDetails(Details details) {
		this.details = details;
	}
	


}
