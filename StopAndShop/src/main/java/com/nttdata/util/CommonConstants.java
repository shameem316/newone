 package com.nttdata.util;

public interface CommonConstants {

	public static final String userIdSession = "userId";
	public static final String loginFailureModel = "msg1";
	public static final String loginFailure ="Authentication Failed, Sorry Please try again to Login";
	public static final String welcomeMsgModel = "headerMsg";
	public static final String welcomeMsg = "Welcome to Stop&Shop Portal";
	public static final String loginQuery= "SELECT user_id FROM account_details WHERE username = :username and password = :password";
	public static final String loginByUnameAndPwd = "loginByUnameAndPwd";
	public static final String loginUname = "username";
	public static final String loginPwd = "password";
	public static final String accountDetails = "Account_Details";
	public static final String loginPage = "login";
	public static final String homePage = "Homepage";
	
	public static final String signUpSuccessModel = "msgSignup";
	public static final String signUpSuccess = "Welcome! you have signed up successfully..";
	public static final String signUpFailureModel = "msgfailSignup";
	public static final String signUpFailure = "Oops, Sorry sign-up was not Successful, please try again or you can write to us on 'contact_us@stopandshop.com'.. ";
	public static final String createAccountPage = "create-account";
	
	public static final String contactSuccessModel = "contactmsg";
	public static final String contactSuccess = "All the Details have been Successfully saved, We will Get In Touch soon.. ";
	public static final String contactFailureModel = "contactfailmsg";
	public static final String contactFailure = "Oops, Sorry the Details have not been saved, you can write to us on 'contact_us@stopandshop.com'.. ";
	public static final String contactUs = "contact_us"; 
	public static final String contactUsPage = "contact_us";
	public static final String failureModel = "msg";
	public static final String failureMsg = "some null pointer exception";
	public static final String failurePage = "failure";
	public static final String failureHibernateMsg = "Some hibernate exception have occurred";
	
	public static String searchShoes = "SELECT * FROM formalshoe WHERE MATCH(product_id,brand,category_id,description,newest,popularity,price,subcategory_id) AGAINST ('";
	public static String searchJeans = "SELECT * FROM jeans WHERE MATCH(product_id,brand,category_id,description,newest,popularity,price,subcategory_id) AGAINST ('";
	public static String searchClocks = "SELECT * FROM clocks WHERE MATCH(product_id,brand,category_id,description,newest,popularity,price,subcategory_id) AGAINST ('";
	public static String searchWatches = "SELECT * FROM watches WHERE MATCH(product_id,brand,category_id,description,newest,popularity,price,subcategory_id) AGAINST ('";
	public static String searchToys = "SELECT * FROM softtoys WHERE MATCH(product_id,brand,category_id,description,newest,popularity,price,subcategory_id) AGAINST ('";
	public static String searchBooks = "SELECT * FROM fictionalbooks WHERE MATCH(product_id,brand,category_id,description,newest,popularity,price,subcategory_id) AGAINST ('";

	public static String sortByPrice0to1000 = " t WHERE t.price >=0 and t.price <=1000";
	public static String sortByPrice1001to2000 = " t WHERE t.price >=1000 and t.price <=2000";
	public static String sortByPrice2000above = " t WHERE t.price >=2000";
	
	public static String deletecart = "delete from cart_table  where user_id =  :userId";

	
	
	
	
	
}
