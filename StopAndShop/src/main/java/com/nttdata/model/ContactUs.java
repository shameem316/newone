package com.nttdata.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.nttdata.util.CommonConstants;

@Entity
@Table(name=CommonConstants.contactUs)
public class ContactUs {

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Column(name="Name") 
	private String name;
	@Column(name="Email") 
	private String email;
	@Column(name="Phone") 
	private String phone;
	@Column(name="Subject") 
	private String subject;
	@Column(name="Message") 
	private String msg;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
	public ContactUs() {
		super();
	}
	
	public ContactUs(String name, String email, String phone, String subject, String msg) {
		super();
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.subject = subject;
		this.msg = msg;
	}
	
}