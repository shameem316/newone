package com.nttdata.model;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import com.nttdata.util.CommonConstants;

@Entity
@Table(name= CommonConstants.accountDetails)
@NamedNativeQueries(
		{
			@NamedNativeQuery(name= CommonConstants.loginByUnameAndPwd, query= CommonConstants.loginQuery )
		}
	)
public class SignUp
{ 
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int user_id;
	private String username;
	private String firstname;
	private String lastname;
	private String email;
	private String password;
	private String phone;
	
	
	public SignUp(String username, String firstname, String lastname, String email, String password, String phone) {
		super();
		this.username = username;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.password = password;
		this.phone = phone;
	}
	public SignUp() {
		super();
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	}