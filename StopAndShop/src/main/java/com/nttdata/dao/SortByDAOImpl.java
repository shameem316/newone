package com.nttdata.dao;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nttdata.model.Clocks;
import com.nttdata.model.FictionalBooks;
import com.nttdata.model.FormalShoe;
import com.nttdata.model.Jeans;
import com.nttdata.model.SoftToys;
import com.nttdata.model.Watches;
import com.nttdata.util.CommonConstants;
import com.nttdata.util.CommonUtil;
import com.nttdata.util.PrototypeBeans;

@Repository
public class SortByDAOImpl implements SortByDAO {

	@Autowired
	private PrototypeBeans prototypeBeans;

	@Autowired
	private HttpServletRequest request;
	@Autowired
	private SessionFactory sf;

	Transaction transaction = null;

	ArrayList list = new ArrayList();

	@Override
	public ArrayList sortByNewest(String category_id) {

		int cid = Integer.parseInt(category_id);

		Criteria cr = null;

		try (Session s = sf.openSession();) {
			if (cid == 1) {
				cr = s.createCriteria(Jeans.class);

			} else if (cid == 2) {
				cr = s.createCriteria(Clocks.class);

			} else if (cid == 3) {
				cr = s.createCriteria(Watches.class);

			} else if (cid == 4) {
				cr = s.createCriteria(FormalShoe.class);

			} else if (cid == 5) {
				cr = s.createCriteria(SoftToys.class);

			} else if (cid == 6) {
				cr = s.createCriteria(FictionalBooks.class);

			}
			transaction = (Transaction) s.beginTransaction();
			cr.addOrder(Order.desc("newest"));
			list = (ArrayList) cr.list();
			transaction.commit();

		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		}
		return list;

	}

	@Override
	public ArrayList sortByPrice0to1000(String category_id) {

		int cid = Integer.parseInt(category_id);
		String tableName = CommonUtil.getTableName(cid);
		try (Session s = sf.openSession();) {
			transaction = (Transaction) s.beginTransaction();
			list = (ArrayList) s.createQuery("FROM " + tableName + CommonConstants.sortByPrice0to1000).getResultList();
			transaction.commit();

		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		}

		return list;

	}

	@Override
	public ArrayList sortByPrice1001to2000(String category_id) {

		int cid = Integer.parseInt(category_id);
		String tableName = CommonUtil.getTableName(cid);

		try (Session s = sf.openSession();) {
			transaction = (Transaction) s.beginTransaction();
			list = (ArrayList) s.createQuery("FROM " + tableName + CommonConstants.sortByPrice1001to2000)
					.getResultList();
			transaction.commit();

		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		}

		return list;

	}

	@Override
	public ArrayList sortByPrice2000above(String category_id) {

		int cid = Integer.parseInt(category_id);
		String tableName = CommonUtil.getTableName(cid);

		try (Session s = sf.openSession();) {
			transaction = (Transaction) s.beginTransaction();
			list = (ArrayList) s.createQuery("FROM " + tableName + CommonConstants.sortByPrice2000above)
					.getResultList();
			transaction.commit();

		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		}

		return list;

	}

}
