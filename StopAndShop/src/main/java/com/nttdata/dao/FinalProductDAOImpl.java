package com.nttdata.dao;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nttdata.util.CommonUtil;

@Repository
public class FinalProductDAOImpl implements FinalProductDAO {
	
	@Autowired
	private SessionFactory sf;
	@Autowired
	private HttpServletRequest request;

	@Override
	public ArrayList productListing(String category_id) throws Exception {

		Transaction transaction = null;

		ArrayList list = new ArrayList();
		int cid = Integer.parseInt(category_id);
		String tableName = CommonUtil.getTableName(cid);

		try (Session s = sf.openSession();) {
			transaction = (Transaction) s. beginTransaction();
			list = (ArrayList) s.createQuery("from " + tableName).getResultList();
			transaction.commit();

		} /*catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		}
*/
		return list;

	}
		
		
		
	
	}

	
