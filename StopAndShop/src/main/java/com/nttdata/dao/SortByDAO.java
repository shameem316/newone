package com.nttdata.dao;

import java.util.ArrayList;

import org.springframework.stereotype.Repository;
@Repository
public interface SortByDAO {
	public ArrayList sortByNewest(String category_id) ;
	
	public ArrayList sortByPrice0to1000(String category_id) ;
	
	public ArrayList sortByPrice1001to2000(String category_id) ;
	
	public ArrayList sortByPrice2000above(String category_id) ;
}
