package com.nttdata.dao;

import javax.persistence.Query;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nttdata.model.CartEntity;
import com.nttdata.request.CartRQ;

@Repository
public class AddToCartDaoImpl implements AddToCartDao {

	@Autowired
	SessionFactory sf;
	@Autowired
	private CartEntity cartEntity ;

	public boolean addtocart(CartRQ cartRQ) {

	
		cartEntity.setUser_id(cartRQ.getUser_id());
		cartEntity.setCategory_id(cartRQ.getCategory_id());
		cartEntity.setSubcategory_id(cartRQ.getSubcategory_id());
		cartEntity.setProduct_id(cartRQ.getProduct_id());
		
		//Session s = sf.openSession();

		Transaction transaction = null;
		try(Session s = sf.openSession()) {
			transaction = (Transaction) s.beginTransaction();

			s.save(cartEntity);
			transaction.commit();

		}

		catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} /*finally {
			s.close();

		}*/

		return true;
	}

	////////////////// *clearcart///////////////////
	public boolean clearcart(CartRQ CartRQ) {
		/*Session s = sf.openSession();*/

		Transaction transaction = null;

		try(Session s = sf.openSession()) {
			transaction = (Transaction) s.beginTransaction();

			int user_id = CartRQ.getUser_id();

			Query query = s.createSQLQuery(" delete from cart_table  where user_id =  :userId");
			query.setParameter("userId", CartRQ.getUser_id());
			/*Query query=s.getNamedNativeQuery("findclearcatr").setParameter("userId", user_id);*/
			System.out.println(query.executeUpdate());
			transaction.commit();

		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} /*finally {
			s.close();

		}*/

		return true;

	}

}
