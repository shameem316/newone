package com.nttdata.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nttdata.request.SearchRQ1;
import com.nttdata.util.CommonConstants;
import com.nttdata.util.CommonUtil;
import com.nttdata.util.PrototypeBeans;

@Repository
public class SearchDAOImpl implements SearchDao {
	@Autowired
	private PrototypeBeans prototypeBeans;
	@Autowired
	SessionFactory sf;

	public List searchResult(SearchRQ1 searchRQ)  {
		String cat = null;
		int flag = 0;
		
		Transaction transaction = null;
		/*Session s = sf.openSession();*/
	
	try(Session s = sf.openSession()) {
		String sear = searchRQ.getSearch();
		if(sear==null)
		{
			return null;
		}
		String currencies[] = sear.split(" ");
		System.out.println(currencies[1]);
		String search = currencies[0];
		String categories[] = { "jeans", "clock", "watch", "shoes", "toys", "books" };
		
		List<Object> list = new ArrayList();
		List productList = new ArrayList();
		/*Transaction transaction = null;
		Session s = sf.openSession();*/

		for (int i = 0; i < 6; i++) {
			if (search.equalsIgnoreCase(categories[i])) {
				cat = categories[i];
			}
		}
		if (cat != null) {

			switch (cat) {

			case "jeans":
				
				transaction = (Transaction) s.beginTransaction();
				String q = CommonConstants.searchJeans + currencies[1] + "')";
				Query query = s.createSQLQuery(q);
				list = query.getResultList();

				list.stream().forEach(obj -> {
					CommonUtil.getProducts(prototypeBeans. getFinalProductRQ(),(ArrayList)productList, obj);
				});
				transaction.commit();
				return productList;

				
			
				

			case "clock":
				
				transaction = (Transaction) s.beginTransaction();
				String q1 = CommonConstants.searchClocks + currencies[1] + "')";
				Query query1 = s.createSQLQuery(q1);
				list = query1.getResultList();

				list.stream().forEach(obj -> {
					CommonUtil.getProducts(prototypeBeans. getFinalProductRQ(),(ArrayList)productList, obj);
				});
				transaction.commit();
				return productList;

			
				

			case "watch":
				
				transaction = (Transaction) s.beginTransaction();
				String q2 = CommonConstants.searchWatches + currencies[1] + "')";
				Query query2 = s.createSQLQuery(q2);
				list = query2.getResultList();

				list.stream().forEach(obj -> {
					CommonUtil.getProducts(prototypeBeans. getFinalProductRQ(),(ArrayList)productList, obj);
				});
				transaction.commit();
				return productList;

				
			

			case "shoes":
				
				
		
				transaction = (Transaction) s.beginTransaction();
				String q3 = CommonConstants.searchShoes + currencies[1] + "')";
				Query query3 = s.createSQLQuery(q3);
				list = query3.getResultList();

				list.stream().forEach(obj -> {
					CommonUtil.getProducts(prototypeBeans. getFinalProductRQ(),(ArrayList)productList, obj);
				});
				transaction.commit();
				return productList;


			
			case "toys":
		
				transaction = (Transaction) s.beginTransaction();
				String q4 = CommonConstants.searchToys + currencies[1] + "')";
				Query query4 = s.createSQLQuery(q4);
				list = query4.getResultList();

				list.stream().forEach(obj -> {
					CommonUtil.getProducts(prototypeBeans. getFinalProductRQ(),(ArrayList)productList, obj);
				});
				transaction.commit();
				return productList;

				
			
			case "books":
				
			
				transaction = (Transaction) s.beginTransaction();
				String q5 = CommonConstants.searchBooks + currencies[1] + "')";
				Query query5 = s.createSQLQuery(q5);
				list = query5.getResultList();

				list.stream().forEach(obj -> {
					CommonUtil.getProducts(prototypeBeans. getFinalProductRQ(),(ArrayList)productList, obj);
				});
				transaction.commit();
				return productList;

				
				
			
			}
		}

		else {

			System.out.println("please enter the category name first");

			return null;
		}
	}
	catch(ArrayIndexOutOfBoundsException e) {
		return null;
		
	}
	catch (HibernateException e) {
		transaction.rollback();
		System.out.println(e);
	} /*finally {
		s.close();

	}*/
		
		System.out.println("sdfhjk");
		return null;

		
	}

}
