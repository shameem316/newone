package com.nttdata.dao;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.nttdata.model.SignUp;

@Repository
public class SignUpDAOImpl implements SignUpDAO
{
	@Autowired
	SessionFactory sf;
	
	public int signUpDAO(SignUp s) 
	{
		int result=0;
	
		try(Session session = sf.openSession()) {
			session.beginTransaction();
			session.save(s);
		    session.getTransaction().commit();
	} 

	catch (HibernateException e) 
	{
		System.out.println("catch in signup: "+e);

	}
	return 1;

}
}

