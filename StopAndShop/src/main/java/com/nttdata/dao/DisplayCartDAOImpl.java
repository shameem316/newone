package com.nttdata.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nttdata.request.DisplayCartRQ;
import com.nttdata.util.PrototypeBeans;

@Repository
public class DisplayCartDAOImpl implements DisplayCartDAO {
	
	      
		
	@Autowired
	SessionFactory sf;
	@Autowired
	private PrototypeBeans prototypeBeans;
	
	 @Autowired HttpSession session;
	 
	@Autowired
	HttpServletRequest request;



	@Override
	public ArrayList displayCart() throws Exception {

		HttpSession session = request.getSession(false);
		int userId = (Integer) session.getAttribute("userId");
		System.out.println(userId + "there");
		String categoryId = (String) session.getAttribute("category_id");
		
		String SubcategoryId = (String) session.getAttribute("subcategory_id");
		String productId = (String) session.getAttribute("product_id");

		List<Object> list = new ArrayList();
		List<DisplayCartRQ> productList = new ArrayList<>();

		Session s = sf.openSession();

		ArrayList<String> res = (ArrayList<String>) s.createSQLQuery("select category_id from cart_table").getResultList();
		
		List<String> catList = new ArrayList<>();
		
		res.stream().forEach(e -> {catList.add((String)e);});
		 
		String obj[]= {"0","1","2","3","4","5","6"};
		HashSet<String> hs = new HashSet<String>(catList);
		Object[] hash = (Object[])hs.toArray();
		

		for (int i=0;i<hs.size();i++) {
			
			String table[] = { "", "jeans", "clocks", "watches", "formalshoe", "softtoys",
			"fictionalbooks" };
			String tableName;
			if(((String)hash[i]).equalsIgnoreCase(obj[1]))
			{
				tableName=table[1];
				System.out.println("--------------true-------------");
			}
			else if(((String)hash[i]).equalsIgnoreCase(obj[2])){
				tableName=table[2];
				System.out.println("--------------true-------------");
			}
			else if(((String)hash[i]).equalsIgnoreCase(obj[3])){
				tableName=table[3];
				System.out.println("--------------true-------------");
				
			}
			else if(((String)hash[i]).equalsIgnoreCase(obj[4])){
				tableName=table[4];
				System.out.println("--------------true-------------");
			}
			else if(((String)hash[i]).equalsIgnoreCase(obj[5])){
				tableName=table[5];
				System.out.println("--------------true-------------");
			}
			else if(((String)hash[i]).equalsIgnoreCase(obj[6])){
				tableName=table[6];
				System.out.println("--------------true-------------");
			}
			else
			{
				tableName=null;
			}
		
			try {
				Transaction transaction = null;
				transaction = s.beginTransaction();
			

				String q = " select c.user_id,c.category_id,c.subcategory_id,c.product_id,f.price, f.brand from cart_table c inner join "
						+ tableName
						+ " f on c.category_id = f.category_id and c.subcategory_id = f.subcategory_id and c.product_id = f.product_id where user_id ="
						+ userId + " ;";
				Query query = s.createSQLQuery(q);
				list = query.getResultList();
				System.out.println("----------list size-----------"+list.size());
				/*for (Object object : res) {
					System.out.println("---------------------"+object);*/
				
				/* query.stream().forEach(o -> { Object obj = o; Object[] object =
				  (Object[]) obj; DisplayCartRQ c = new DisplayCartRQ();
				  c.setUser_id((Integer) object[0]); c.setCategory_id((String)
				  object[1]); c.setSubcategory_id((String) object[2]);
				  c.setProduct_id((String) object[3]); list1.add(c); });
				 */

			list.stream().forEach(e-> {
					Object o[] = (Object[]) e;
					DisplayCartRQ cr = new DisplayCartRQ();
					cr.setUser_id((Integer) o[0]);
					cr.setCategory_id((String) o[1]);
					cr.setSubcategory_id((String) o[2]);
					cr.setProduct_id((String) o[3]);
					cr.setPrice((String) o[4]);
					cr.setBrand((String) o[5]);
					
					productList.add(cr);
				});
			 System.out.println("-----------prod list-------------"+productList);

				//}

				s.getTransaction().commit();
				

		} catch (HibernateException e) {

				e.printStackTrace();
			}
			catch(NullPointerException n)
			{
				n.printStackTrace();
			}
				
			
		}


		
		
		
		System.out.println("*************************************** "+productList.size());

		return  (ArrayList)productList;
	

}}
