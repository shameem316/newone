package com.nttdata.dao;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.nttdata.model.ContactUs;


@Repository
public class ContactUsDAOImpl implements ContactUsDAO {

	@Autowired
	SessionFactory sf;

	public int contactDAO(ContactUs contact)
	{
		try(Session session = sf.openSession()) {
			
			session.beginTransaction();
			session.save(contact);
			session.getTransaction().commit(); 
		}
		catch (HibernateException e) 
		{
			System.out.println("catch in signup: "+e);

		}
		return 1;
	}

}
