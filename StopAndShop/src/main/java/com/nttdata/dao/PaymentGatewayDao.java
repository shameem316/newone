package com.nttdata.dao;

import com.nttdata.request.PaymentGatewayRQ;
@FunctionalInterface
public interface PaymentGatewayDao {
public boolean payDao(PaymentGatewayRQ p);

}
