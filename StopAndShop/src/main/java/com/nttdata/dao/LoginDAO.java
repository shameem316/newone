package com.nttdata.dao;
import com.nttdata.request.LoginRQ;

public interface LoginDAO {
	public int authenticateDAO(LoginRQ loginRQ);
}
