package com.nttdata.dao;
import java.util.List;
import java.util.Optional;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.nttdata.request.LoginRQ;
import com.nttdata.util.CommonConstants;
import com.nttdata.util.CommonUtil;

@Repository
public class LoginDAOImpl implements LoginDAO {
	
	@Autowired
	SessionFactory sf;

	public int authenticateDAO(LoginRQ loginRQ) 
	{
		int userId=0;

		try(Session s=sf.openSession()) {
 
			 s.beginTransaction();
			 Query query = s.getNamedNativeQuery(CommonConstants.loginByUnameAndPwd);
			 query.setParameter(CommonConstants.loginUname, loginRQ.getUsername());
			 query.setParameter(CommonConstants.loginPwd, CommonUtil.enCode(loginRQ.getPassword()));	
			 List list = query.list();
			 s.getTransaction().commit();
			
			 Optional<List> checkNull = Optional.ofNullable(list);  

			 if(checkNull.isPresent())
			 {
				 userId=(int) list.get(0);
				 return userId;
			 }		 
			 
	}
		catch (HibernateException e) 
		{
			System.out.println("catch in signup: "+e);
		}
return 0;
	
}}