package com.nttdata.request;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value="prototype")
public class FinalProductRQ {
	private String category_id;
	private String subcategory_id;
	private String product_id;
	private String brand;
	private String price;
	private String popularity;
	private String newest;
	private String description;
	public FinalProductRQ(String category_id, String subcategory_id, String product_id, String brand, String price,
			String popularity, String newest, String description) {
		super();
		this.category_id = category_id;
		this.subcategory_id = subcategory_id;
		this.product_id = product_id;
		this.brand = brand;
		this.price = price;
		this.popularity = popularity;
		this.newest = newest;
		this.description = description;
	}
	public FinalProductRQ() {
		super();
	}
	public String getCategory_id() {
		return category_id;
	}
	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}
	public String getSubcategory_id() {
		return subcategory_id;
	}
	public void setSubcategory_id(String subcategory_id) {
		this.subcategory_id = subcategory_id;
	}
	public String getProduct_id() {
		return product_id;
	}
	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getPopularity() {
		return popularity;
	}
	public void setPopularity(String popularity) {
		this.popularity = popularity;
	}
	public String getNewest() {
		return newest;
	}
	public void setNewest(String newest) {
		this.newest = newest;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	

}
