package com.nttdata.request;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//@Component
@Scope(value="prototype")
public class Details {
	private String price;
	private String brand;

	
	public Details(String price, String brand) {
		this.price = price;
		this.brand = brand;
	}

	
	public Details() {
	
	}


	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	
	
	

}
