package com.nttdata.request;

public class LoginRQ {

	private String username;
	private String password;
	private int user_id;
	
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public LoginRQ(int user_id) {
		super();
		this.user_id = user_id;
	}
	public LoginRQ() {
		super();
	}
	public LoginRQ(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	public LoginRQ(String username, String password, int user_id) {
		super();
		this.username = username;
		this.password = password;
		this.user_id = user_id;
	}

	

}
