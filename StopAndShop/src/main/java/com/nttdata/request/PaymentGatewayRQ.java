package com.nttdata.request;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value="prototype")
public class PaymentGatewayRQ {
private int userId;
private int categoryId;
private int productId;
private String cardname;
public PaymentGatewayRQ() {
	super();
}
public int getUserId() {
	return userId;
}
public void setUserId(int userId) {
	this.userId = userId;
}
public int getCategoryId() {
	return categoryId;
}
public void setCategoryId(int categoryId) {
	this.categoryId = categoryId;
}
public int getProductId() {
	return productId;
}
public void setProductId(int productId) {
	this.productId = productId;
}
public String getCardname() {
	return cardname;
}
public void setCardname(String cardname) {
	this.cardname = cardname;
}


}
