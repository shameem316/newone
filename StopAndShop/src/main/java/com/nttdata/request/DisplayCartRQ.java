package com.nttdata.request;



import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component
@Scope(value = "prototype")

public class DisplayCartRQ extends Details  {
	private int user_id;
	private String category_id;
	private String subcategory_id;
	private String product_id;
	public DisplayCartRQ(int user_id, String category_id, String subcategory_id, String product_id) {
		
		this.user_id = user_id;
		this.category_id = category_id;
		this.subcategory_id = subcategory_id;
		this.product_id = product_id;
	}
	public DisplayCartRQ() {
		
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getCategory_id() {
		return category_id;
	}
	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}
	public String getSubcategory_id() {
		return subcategory_id;
	}
	public void setSubcategory_id(String subcategory_id) {
		this.subcategory_id = subcategory_id;
	}
	public String getProduct_id() {
		return product_id;
	}
	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}

	
	
}
